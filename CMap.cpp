//
// Copyright (C) 2017 Project Garibaldi
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software Foundation,
// Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
//

#include "CMap.h"

#include <Urho3D/Core/Context.h>
#include <Urho3D/Graphics/Texture2D.h>
#include <Urho3D/Urho2D/Sprite2D.h>
#include <Urho3D/Urho2D/StaticSprite2D.h>

int CMap::Setup()
{
	// Get sprites
	Sprite2D* terrainSprite = cache_->GetResource<Sprite2D>("Maps/terrain-bg.png");
	if (!terrainSprite)
		return -1;
	Sprite2D* borderSprite = cache_->GetResource<Sprite2D>("Maps/terrain-borders.png");
	if (!borderSprite)
		return -1;
	colouredMap_ = cache_->GetResource<Image>("Maps/coloured_provinces.png");
	if (!colouredMap_ || !colouredMap_->FlipVertical())
		return -1;
	politicalMap_ = cache_->GetResource<Image>("Maps/political_map.png");
	if (!politicalMap_)
		return -1;
	
	terrainNode_ = scene_->CreateChild("terrainMap");
	terrainNode_->SetPosition(Vector3(0.0f, 0.0f, 0.0f));
	StaticSprite2D* staticTerrainSprite = terrainNode_->CreateComponent<StaticSprite2D>();
	staticTerrainSprite->SetSprite(terrainSprite);
	staticTerrainSprite->SetOrderInLayer(0);

	borderNode_ = scene_->CreateChild("borderMap");
	borderNode_->SetPosition(Vector3(0.0f, 0.0f, 0.0f));
	StaticSprite2D* staticBorderSprite = borderNode_->CreateComponent<StaticSprite2D>();
	staticBorderSprite->SetSprite(borderSprite);
	staticBorderSprite->SetOrderInLayer(1); // Draw borders on top of the terrain

	// Draw a political map
	ProvinceMap::Iterator it = provinces_->Begin();
	ProvinceMap::Iterator end = provinces_->End();
	for (; it != end; ++it) {
		FillWithColourFromPixel(it->second_);
	}
	politicalMap_->SavePNG("/tmp/political_map.png");

	//Texture2D *t = new Texture2D(context_); // both these lines are okay, which is nicer?
	SharedPtr<Texture2D> t = context_->CreateObject<Texture2D>();
	if (!t)
		return -1;
	
	t->SetData(politicalMap_);
	t->SetName("politicalTexture");
	cache_->AddManualResource(t.Get());

	//Sprite2D* politicalSprite = new Sprite2D(context_); // both these lines are okay, which is nicer?
	SharedPtr<Sprite2D> politicalSprite = context_->CreateObject<Sprite2D>();
	if (!politicalSprite)
		return -1;

	politicalSprite->SetTexture(t.Get());
	politicalSprite->SetRectangle(IntRect(0, 0, t->GetWidth(), t->GetHeight()));
	politicalSprite->SetName("politicalSprite");
	cache_->AddManualResource(politicalSprite);

	// Display political map
	politicalNode_ = scene_->CreateChild("politcalMap");
	politicalNode_->SetPosition(Vector3(0.0f, 0.0f, 0.0f));
	StaticSprite2D* staticPoliticalSprite = politicalNode_->CreateComponent<StaticSprite2D>();
	staticPoliticalSprite->SetSprite(politicalSprite);
	staticPoliticalSprite->SetOrderInLayer(2);
}

unsigned CMap::GetPixelInt(int x, int y)
{
	colouredMap_->GetPixelInt(x, y);
}

bool CMap::IsNodeHit(Node *n)
{
	 if (n == borderNode_.Get() || n == politicalNode_.Get())
		return true;

	return false;
}

void CMap::FillWithColourFromPixel(CProvince &p)
{
	int startx = p.centerI.x_;
	int starty = p.centerI.y_;
	origColour = politicalMap_->GetPixelInt(startx, starty);
	ownerColour = nations_->Find(p.ownerTag)->second_.polColour & 0x6fffffff;
	
	if (p.ownerTag == p.occupantTag) {
		FloodFillSimple(startx, starty);
	} else {
		occupantColour = nations_->Find(p.occupantTag)->second_.polColour & 0x6fffffff;
		FloodFillDual(startx, starty);
	}
}

void CMap::FloodFillSimple(int startx, int starty)
{
	unsigned c = politicalMap_->GetPixelInt(startx, starty);
	if (c != origColour) {
		return;
	}
	
	politicalMap_->SetPixelInt(startx, starty, ownerColour);
	FloodFillSimple(startx -1, starty);
	FloodFillSimple(startx, starty -1);
	FloodFillSimple(startx +1, starty);
	FloodFillSimple(startx, starty +1);
}

void CMap::FloodFillDual(int startx, int starty)
{
	if (politicalMap_->GetPixelInt(startx, starty) != origColour) {
		return;
	}
	
	if ((startx + starty) % 30 < 10) {
		politicalMap_->SetPixelInt(startx, starty, occupantColour);
	} else {
		politicalMap_->SetPixelInt(startx, starty, ownerColour);
	}
	FloodFillDual(startx -1, starty);
	FloodFillDual(startx, starty -1);
	FloodFillDual(startx +1, starty);
	FloodFillDual(startx, starty +1);
}


