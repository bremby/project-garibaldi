//
// Copyright (C) 2017 Project Garibaldi
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software Foundation,
// Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
//

#include "CStockpile.h"

CStockpile::CStockpile()
{
	for (int r = EResource::RES_COAL; r != EResource::RES_LAST; r++) {
		resources[r] = 0;
	}
}

unsigned CStockpile::AddResource(EResource r, unsigned amount)
{
	return resources[r] += amount;
}


unsigned CStockpile::RemResource(EResource r, unsigned amount)
{
	return resources[r] -= amount;
}

bool CStockpile::ConsumeAndProduce(Vector<Commodity> inputs, Vector<Commodity> outputs)
{
	Vector<Commodity>::Iterator it = inputs.Begin();
	for(; it != inputs.End(); ++it) {
		if (resources[it->first_] < it->second_)
			return false;
	}

	for(it = inputs.Begin(); it != inputs.End(); ++it) {
		RemResource(it->first_, it->second_);
	}
	for(it = outputs.Begin(); it != outputs.End(); ++it) {
		AddResource(it->first_, it->second_);
	}

	return true;
}
