//
// Copyright (C) 2017 Project Garibaldi
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software Foundation,
// Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
// 

#include "CProvince.h"

#include <Urho3D/Container/ForEach.h>
#include <Urho3D/IO/Log.h>
#include <Urho3D/Urho2D/Drawable2D.h>

CProvince::CProvince(String name, unsigned colour, Log *log, int centerX, int centerY)
 : name(name), colour(colour), log(log), ownerTag(0), occupantTag(0)
{
	centerI.x_ = centerX;
	centerI.y_ = centerY;

	centerF.x_ = PIXEL_SIZE * centerX-10.0f; // magic
	centerF.y_ = -1.0f * (PIXEL_SIZE * centerY-10.0f);
	centerF.z_ = 0.0f;
}

void CProvince::Process()
{
	Vector<CProducer>::Iterator pit = producers.Begin();
	for(; pit != producers.End(); ++pit) {
		if (!stockpile.ConsumeAndProduce(pit->inputs, pit->outputs))
			log->URHO3D_LOGDEBUGF("Province %s unable to produce due to lack of resources!", name.CString());
	}
}
