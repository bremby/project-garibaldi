import cv2
import numpy as np

# Read image
img = cv2.imread('coloured_provinces2.png',1)

gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
thresh = cv2.threshold(gray, 1, 255, cv2.THRESH_BINARY)[1]
cnts = cv2.findContours(thresh, cv2.RETR_EXTERNAL,
	cv2.CHAIN_APPROX_SIMPLE)
cnts = cnts[0] if cv2.__version__.split(".") == '2' else cnts[1]

nothing = [0,0,0]

# loop over the contours
for c in cnts:
	# compute the center of the contour
	M = cv2.moments(c)
	cX = int(M["m10"] / M["m00"])
	cY = int(M["m01"] / M["m00"])
	p = img[cX][cY]
	# we might need some better filters, as this may output a center for a province twice (when dealing with very concave shapes)
	if not np.all(p == nothing):
		print("{:02x}{:02x}{:02x} centered at {}x{}".format(p[2],p[1],p[0],cX,cY))
 
