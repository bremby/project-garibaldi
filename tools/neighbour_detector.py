import cv2
import numpy as np
from matplotlib import pyplot as plt

img = cv2.imread('coloured_provinces2.png',1)
edges = cv2.Canny(img,0,2)

nothing = [0,0,0]

def test_points(p1, p2):
	if (not np.all(p1 == p2)
	   and not np.all(p1 == nothing)
	   and not np.all(p2 == nothing)):
		if (p1[0] > p2[0] or p1[1] > p2[1] or p1[2] > p2[2]):
			tmp = p1
			p1 = p2
			p2 = tmp
		print("{:02x}{:02x}{:02x} neighbours {:02x}{:02x}{:02x}".format(p1[2],p1[1],p1[0],p2[2],p2[1],p2[0]))

for x in range(5, len(edges) - 5):
	for y in range(5, len(edges[0]) - 5):
		if edges[x][y] != 0:
			if x >= 5 and x < len(edges) - 5:
				test_points(img[x-5][y], img[x+5][y])
			if y >= 5 and y < len(edges[0]) - 5:
				test_points(img[x][y-5], img[x][y+5])
		
