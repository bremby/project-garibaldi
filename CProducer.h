//
// Copyright (C) 2017 Project Garibaldi
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software Foundation,
// Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
//

#pragma once

#include <Urho3D/Container/Vector.h>

#include "CStockpile.h"

using namespace Urho3D;

class CProducer
{
public:
	//void Produce(); // we need list of province-based effects, stockpile for inputs and a way to express the resource of production
	void AddInput(EResource, unsigned);
	void AddOutput(EResource, unsigned);

public:
	Vector<Commodity> inputs;
	Vector<Commodity> outputs;

	// there should be a list of applied effects here
};
