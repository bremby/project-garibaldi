//
// Copyright (C) 2017 Project Garibaldi
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software Foundation,
// Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
//


#include "CResource.h"


const char *ResourceNames[] = {
	"coal",
	"grain",
	"fish",
	"iron",
	"steel",
	"smallarms",
	"lumber",
	"furniture",
	NULL
};

const char *getResourceName(EResource r) {
	return ResourceNames[r];
}

EResource getResourceEnum(const char *name) {
	int i;
	for (i = (int) RES_COAL; i < RES_LAST; ++i) {
		if (strcmp(ResourceNames[i], name) == 0)
			break;
	}
	return (EResource) i;
}

CResource::CResource(unsigned key, String name, float value)
	: key(key), name(name), value(value)
{
}

bool CResource::operator!=(const CResource a)
{
	return (a.name.Compare(name) != 0) || (a.key != key) || (a.value != value);
}
