//
// Copyright (C) 2017 Project Garibaldi
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software Foundation,
// Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
//

#pragma once

#include <Urho3D/Container/HashMap.h>
#include <Urho3D/Container/Str.h>

#include <cstring>

using namespace Urho3D;

/* The following two listings must be kept in the same ordering!" */

typedef enum ERes {
	RES_COAL = 0,
	RES_GRAIN,
	RES_FISH,
	RES_IRON,
	RES_STEEL,
	RES_SMALLARMS,
	RES_LUMBER,
	RES_FURNITURE,
	RES_LAST // this MUST be the last resource!
} EResource;

const char *getResourceName(EResource r);

EResource getResourceEnum(const char *name);

class CResource
{
public:
	CResource() {};
	CResource(unsigned key, String name, float value);

	bool operator!=(const CResource a);

	unsigned GetKey() { return key;  }
	String GetName() { return name; }
	float GetValue() { return value; }
private:
	unsigned key;
	String name;
	float value;
};
