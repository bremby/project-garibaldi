# PROJECT GARIBALDI

A community-built grand strategy game.

Building
--------

1. Build Urho3D outside this folder yourself. Guides are here: 
https://github.com/urho3d/Urho3D/wiki


2. Run the preferred `cmake_*` script to setup the project. This is 
required to do everytime files are added, removed, or moved/renamed. On 
bare Linux use `cmake_generic.sh`, on Windows probably 
`cmake_generic.bat`. Unless you install Urho3D to a system-wide 
location, you need to supply an environment variable or a command-line 
option telling the scripts where to find the library. For example, 
supposing Urho3D was built in the `~/Urho3D-1.6-build` folder:

```
./cmake_generic.sh ./build -D URHO3D_HOME=$HOME/Urho3D-1.6-build
```

3. Compile using the generated Makefile.

```
cd build
make
```

