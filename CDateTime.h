//
// Copyright (C) 2017 Project Garibaldi
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software Foundation,
// Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
//

#pragma once
#include <Urho3D/Core/Object.h>
#include <Urho3D/Container/Str.h>

using namespace Urho3D;

class URHO3D_API CDateTime : public Object
{
	URHO3D_OBJECT(CDateTime, Object);

public:
	CDateTime(Context* context);

	void SetDate(int year, int month, int day);
	void SetDay(int day);
	void SetMonth(int month);
	void SetYear(int year);
	void SetTime(int hour, int minute, int second);
	void SetHour(int hour);
	void SetMinute(int minute);
	void SetSecond(int second);

	String GetDate();
	String GetTime();

	inline int GetYear() {
		return year;
	}

	inline int GetMonth() {
		return month;
	}

	inline int GetDay() {
		return day;
	}

	inline int GetHour() {
		return hour;
	}

	void Update(float delta);
	bool IsLeapYear();

private:
	float deltaPerUpdate;
	float deltaSinceUpdate;
	int timeAddedOnUpdate;

	int second;
	int minute;
	int hour;
	int day;
	int month;
	int year;

	int daysInMonth[12] = { 31,28,31,30,31,30,31,31,30,31,30,31 };
	int leapDaysInMonth[12] = { 31,29,31,30,31,30,31,31,30,31,30,31 };
};
