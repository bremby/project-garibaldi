//
// Copyright (C) 2017 Project Garibaldi
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software Foundation,
// Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
//

#pragma once

#include <Urho3D/Container/Str.h>
#include <Urho3D/Scene/Node.h>
#include <Urho3D/Urho2D/StaticSprite2D.h>

using namespace Urho3D;

typedef enum {
	UT_HQ,
	UT_BRIGADE
} EUnitType;

class CUnit
{
public:
	CUnit() { type = UT_BRIGADE; }
	CUnit(EUnitType type) : type(type) {}

public:
	EUnitType type;
	String name;
	unsigned manpower;
	SharedPtr<Node> node;
	SharedPtr<StaticSprite2D> sprite;
};
