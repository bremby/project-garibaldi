{
  "map": {
    "year": 1877,
    "provinces": [
      {
        "name": "void",
        "colour": "000000"
      },
      {
        "name": "Trst",
        "colour": "010000"
      },
      {
        "name": "Nabrežina",
        "colour": "020000"
      },
      {
        "name": "Koper",
        "colour": "030000"
      },
      {
        "name": "Izola",
        "colour": "040000"
      },
      {
        "name": "Umag",
        "colour": "050000"
      },
      {
        "name": "Poreč",
        "colour": "060000"
      },
      {
        "name": "Pula",
        "colour": "070000"
      },
      {
        "name": "Pazin",
        "colour": "080000"
      },
      {
        "name": "Rabac",
        "colour": "090000"
      },
      {
        "name": "Opatija",
        "colour": "0a0000"
      },
      {
        "name": "Žejane",
        "colour": "0b0000"
      },
      {
        "name": "Rijeka",
        "colour": "0c0000"
      },
      {
        "name": "Delnice",
        "colour": "0d0000"
      },
      {
        "name": "Krk",
        "colour": "0e0000"
      },
      {
        "name": "Cres",
        "colour": "0f0000"
      },
      {
        "name": "Senj",
        "colour": "100000"
      },
      {
        "name": "Mt. Velebit",
        "colour": "110000"
      },
      {
        "name": "Gospić",
        "colour": "120000"
      },
      {
        "name": "Zadar",
        "colour": "130000"
      },
      {
        "name": "Šibenik",
        "colour": "140000"
      },
      {
        "name": "Trogir",
        "colour": "150000"
      },
      {
        "name": "Split",
        "colour": "160000"
      },
      {
        "name": "Makarska",
        "colour": "170000"
      },
      {
        "name": "Metković",
        "colour": "180000"
      },
      {
        "name": "Gradac",
        "colour": "190000"
      },
      {
        "name": "Pelješac",
        "colour": "1a0000"
      },
      {
        "name": "Dubrovnik",
        "colour": "1b0000"
      },
      {
        "name": "Hvar",
        "colour": "1c0000"
      },
      {
        "name": "Brač",
        "colour": "1d0000"
	  },
	  {
		"name": "Herceg Novi",
        "colour": "1e0000"
      },
      {
        "name": "Sinj",
        "colour": "1f0000"
      },
      {
        "name": "Trilj",
        "colour": "200000"
      },
      {
        "name": "Imotski",
        "colour": "210000"
      },
      {
        "name": "Mt. Svilaja",
        "colour": "220000"
      },
      {
        "name": "Knin",
        "colour": "230000"
      },
      {
        "name": "Skradin",
        "colour": "240000"
      },
      {
        "name": "Gračac",
        "colour": "250000"
      },
      {
        "name": "Udbina",
        "colour": "260000"
      },
      { 
		"name": "Donji Lapac",
        "colour": "270000"
      },
      {
        "name": "Plitvica",
        "colour": "280000"
      },
      {
        "name": "Otočac",
        "colour": "290000"
      },
      {
        "name": "Slunj",
        "colour": "2a0000"
      },
      {
        "name": "Josipdol",
        "colour": "2b0000"
      },
      {
        "name": "Karlovac",
        "colour": "2c0000"
      },
      {
        "name": "Samobor",
        "colour": "2d0000"
      },
      {
        "name": "Varaždin",
        "colour": "2e0000"
      },
      {
        "name": "Čakovec",
        "colour": "2f0000"
      },
      {
        "name": "Bjelovar",
        "colour": "300000"
      },
      {
        "name": "Zagreb",
        "colour": "310000"
      },
      {
        "name": "Sisak",
        "colour": "320000"
      },
      {
        "name": "Petrinja",
        "colour": "330000"
      },
      {
        "name": "Dvor",
        "colour": "340000"
      },
      {
        "name": "Kostajnica",
        "colour": "350000"
      },
      {
        "name": "Kutina",
        "colour": "360000"
      },
      {
        "name": "Nova Gradiška",
        "colour": "370000"
      },
      {
        "name": "Daruvar",
        "colour": "380000"
      },
      {
        "name": "Vitrovica",
        "colour": "390000"
      },
      {
        "name": "Požega",
        "colour": "3a0000"
      },
      {
        "name": "Slavonski Brod",
        "colour": "3b0000"
      },
      {
        "name": "Levanjska Varoš",
        "colour": "3c0000"
      },
      {
        "name": "Osijek",
        "colour": "3d0000"
      },
      {
        "name": "Đakovo",
        "colour": "3e0000"
      },
      {
        "name": "Vukovar",
        "colour": "3f0000"
      },
      {
        "name": "Vinkovci",
        "colour": "400000"
      }
    ]
  }
}