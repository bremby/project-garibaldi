//
// Copyright (C) 2017 Project Garibaldi
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software Foundation,
// Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
//

#pragma once

#include <Urho3D/Container/HashMap.h>
#include <Urho3D/Core/WorkQueue.h>
#include <Urho3D/Engine/Application.h>
#include <Urho3D/Graphics/Viewport.h>
#include <Urho3D/Input/Input.h>
#include <Urho3D/UI/Window.h>

#include "CProvince.h"
#include "CPop.h"
#include "CDateTime.h"
#include "CResource.h"
#include "CUnit.h"
#include "CNation.h"
#include "CMap.h"

namespace Urho3D
{

class Node;
class Scene;
class Sprite;

}

// All Urho3D classes reside in namespace Urho3D
using namespace Urho3D;

const float TOUCH_SENSITIVITY = 2.0f;

/// Main class, as framework for all samples.
///    - Initialization of the Urho3D engine (in Application class)
///    - Modify engine parameters for windowed mode and to show the class name as title
///    - Create Urho3D logo at screen
///    - Set custom window title and icon
///    - Create Console and Debug HUD, and use F1 and F2 key to toggle them
///    - Toggle rendering options from the keys 1-8
///    - Take screenshot with key 9
///    - Handle Esc key down to hide Console or exit application
///    - Init touch input on mobile platform using screen joysticks (patched for each individual sample)
class Main : public Application
{
	// Enable type information.
	URHO3D_OBJECT(Main, Application);

public:
    typedef HashMap<unsigned, CProvince> ProvinceMap;
	typedef HashMap<unsigned, CResource> ResourceMap;
	typedef HashMap<unsigned, CNation> NationMap;
	typedef HashMap<Node *, CUnit *> UnitMap;

	/// Construct.
	Main(Context* context);

	/// Setup before engine initialization. Modifies the engine parameters.
	virtual void Setup();
	/// Setup after engine initialization. Creates the logo, console & debug HUD.
	virtual void Start();
	/// Cleanup after the main loop. Called by Application.
	virtual void Stop();

	void AddPopToProvince(CPop& pop, unsigned province_key);
	void RemovePopFromProvince(CPop& pop, unsigned province_key);

protected:
	/// Return XML patch instructions for screen joystick layout for a specific sample app, if any.
	virtual String GetScreenJoystickPatchString() const { return
		"<patch>"
		"    <add sel=\"/element/element[./attribute[@name='Name' and @value='Hat0']]\">"
		"        <attribute name=\"Is Visible\" value=\"false\" />"
		"    </add>"
		"</patch>";
	}
	/// Initialize mouse mode on non-web platform.
	void InitMouseMode(MouseMode mode);
	
	/// Scene.
	SharedPtr<Scene> scene_;
	/// Camera scene node.
	SharedPtr<Node> cameraNode_;
	SharedPtr<Viewport> viewport_;
	/// Map scene node.
	CMap *map_;
	SharedPtr<Node> terrainNode_;
	SharedPtr<Node> borderNode_;
	/// Province database.
	ProvinceMap provinces_;
	ResourceMap resources_;
	NationMap nations_;
	SharedPtr<Window> provinceExplorer_;
	SharedPtr<Window> provinceView_;
	SharedPtr<Window> unitView_;
	CProvince* selectedProvince_;
	SharedPtr<WorkQueue> wq_;
	List<CPop> popList_;
	
	HashMap<Node *, CUnit *> units_;
	CUnit *selectedUnit_;
	/// Mouse mode option to use in the sample.
	MouseMode useMouseMode_;

private:
	void CreateScene();
	/// Create logo.
	void CreateLogo();
	/// Sets up the user interface.
	void CreateUI();
	/// Handle loading the game data
	void LoadData();
	/// Load the JSON map configuration.
	void LoadMapConfig(String file);
	/// Load the JSON Pop's
	void LoadPops(String file);
	void LoadResources(String file);
	void LoadNations(String file);
	/// Parse a colour string
	unsigned ParseColour(String colour);
	/// Load JSON
	JSONValue& LoadJSON(String file);
	/// Set custom window Title & Icon
	void SetWindowTitleAndIcon();
	/// Create console and debug HUD.
	//void CreateConsoleAndDebugHud();
	void SetupViewport();
	void MoveCamera(float timeStep);
	/// Handle key down event to process key controls common to all samples.
	void HandleKeyDown(StringHash eventType, VariantMap& eventData);
	/// Handle key up event to process key controls common to all samples.
	void HandleKeyUp(StringHash eventType, VariantMap& eventData);
	void HandleMouseButtonDown(StringHash eventType, VariantMap& eventData);
	void HandleMouseButtonUp(StringHash eventType, VariantMap& eventData);
	/// Handle scene update event to control camera's pitch and yaw for all samples.
	void HandleSceneUpdate(StringHash eventType, VariantMap& eventData);
	/// Subscribe to application-wide logic update events.
	void SubscribeToEvents();
	/// Handle the logic update event.
	void HandleUpdate(StringHash eventType, VariantMap& eventData);

	void HandleDateTimeUpdated(StringHash eventType, VariantMap& eventData);
	void HandleYearUpdated(StringHash eventType, VariantMap& eventData);
	void HandleMonthUpdated(StringHash eventType, VariantMap& eventData);
	void HandleDayUpdated(StringHash eventType, VariantMap& eventData);
	void HandleHourUpdated(StringHash eventType, VariantMap& eventData);

	void HandlePlayPressed(StringHash eventType, VariantMap& eventData);

	void AddProvinceProcessingJobs();
	void UpdateProvinceWindowValues();
	void SelectProvince(CProvince& province);
	void DeselectProvince();
	void UpdateUnitWindowValues();
	void SelectUnit(CUnit& unit);
	void DeselectUnit();

	bool CheckResource(String file);

	/// Pause flag.
	bool paused_;
	/// Camera object.
	Camera* camera_;
	/// Logging.
	Log *log_;

	SharedPtr<CDateTime> dateTime_;
	
	SharedPtr<Image> colouredMap_;
	SharedPtr<Sprite2D> infIcon_;

	int speed_;
    
    unsigned map_key_min;
    unsigned map_key_max;
};

