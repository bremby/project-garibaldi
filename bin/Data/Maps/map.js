{"map": {
  "year": 1877,
  "month": 6,
  "day": 1,
  "provinces": [
    {
      "name": "Gradac",
      "colour": "010000",
      "owner" : 2,
      "centerX" : 825,
      "centerY" : 220,
      "produces" : [
        {
          "out" : {
            "coal" : 5
          }
        }
      ]
    },
    {
      "name": "Pljevlja",
      "colour": "020000",
      "owner" : 2,
      "centerX" : 1030,
      "centerY" : 330,
      "produces" : [
        {
          "out" : {
            "lumber" : 1
          }
        },
        {
          "in" : {
            "lumber" : 1
          },
         "out" : {
            "furniture" : 2
          }
        }
      ]
    },
    {
      "name": "Tomaševo",
      "colour": "030000",
      "owner" : 2,
      "centerX" : 1210,
      "centerY" : 510
    },
    {
      "name": "Bijelo Polje",
      "colour": "040000",
      "owner" : 2,
      "centerX" : 1375,
      "centerY" : 600
    },
    {
      "name": "Ivangrad",
      "colour": "050000",
      "owner" : 2,
      "centerX" : 1480,
      "centerY" : 810
    },
    {
      "name": "Rožaj",
      "colour": "060000",
      "owner" : 2,
      "centerX" : 1730,
      "centerY" : 830
    },
    {
      "name": "Plužine",
      "colour": "070000",
      "owner" : 2,
      "centerX" : 575,
      "centerY" : 490
    },
    {
      "name": "Žabljak",
      "colour": "080000",
      "owner" : 2,
      "centerX" : 835,
      "centerY" : 470
    },
    {
      "name": "Šavnik",
      "colour": "090000",
      "owner" : 2,
      "centerX" : 840,
      "centerY" : 670
    },
    {
      "name": "Mojkovac",
      "colour": "0a0000",
      "owner" : 2,
      "occupant": 3,
      "centerX" : 1150,
      "centerY" : 650
    },
    {
      "name": "Kolašin",
      "colour": "0b0000",
      "owner" : 2,
      "occupant": 3,
      "centerX" : 1175,
      "centerY" : 860
    },
    {
      "name": "Manastir Morača",
      "colour": "0c0000",
      "owner" : 3,
      "centerX" : 1030,
      "centerY" : 915
    },
    {
      "name": "Andrijevica",
      "colour": "0d0000",
      "owner" : 3,
      "centerX" : 1365,
      "centerY" : 965
    },
    {
      "name": "Plav",
      "colour": "0e0000",
      "owner" : 3,
      "centerX" : 1520,
      "centerY" : 1100
    },
    {
      "name": "Nikšić",
      "colour": "0f0000",
      "owner" : 3,
      "centerX" : 665,
      "centerY" : 890
    },
    {
      "name": "Danilovgrad",
      "colour": "100000",
      "owner" : 3,
      "centerX" : 810,
      "centerY" : 1125
    },
    {
      "name": "Podgorica",
      "colour": "110000",
      "owner" : 3,
      "centerX" : 1000,
      "centerY" : 1245
    },
    {
      "name": "Velimlje",
      "colour": "120000",
      "owner" : 3,
      "centerX" : 375,
      "centerY" : 790
    },
    {
      "name": "Grahovo",
      "colour": "130000",
      "owner" : 3,
      "centerX" : 400,
      "centerY" : 1010
    },
    {
      "name": "Cetinje",
      "colour": "140000",
      "owner" : 3,
      "centerX" : 610,
      "centerY" : 1220
    },
    {
      "name": "Rijeka Crnojevića",
      "colour": "150000",
      "owner" : 3,
      "centerX" : 770,
      "centerY" : 1415
    },
    {
      "name": "Kotor",
      "colour": "160000",
      "owner" : 3,
      "centerX" : 460,
      "centerY" : 1185
    },
    {
      "name": "Herceg Novi",
      "colour": "170000",
      "owner" : 3,
      "centerX" : 310,
      "centerY" : 1250
    },
    {
      "name": "Tivat",
      "colour": "180000",
      "owner" : 3,
      "centerX" : 435,
      "centerY" : 1360
    },
    {
      "name": "Budva",
      "colour": "190000",
      "owner" : 3,
      "centerX" : 645,
      "centerY" : 1520
    },
    {
      "name": "Virpazar",
      "colour": "1a0000",
      "owner" : 3,
      "centerX" : 800,
      "centerY" : 1580
    },
    {
      "name": "Bar",
      "colour": "1b0000",
      "owner" : 3,
      "centerX" : 830,
      "centerY" : 1720
    },
    {
      "name": "Ulcinj",
      "colour": "1c0000",
      "owner" : 3,
      "centerX" : 980,
      "centerY" : 1860
    }
  ]
}}
