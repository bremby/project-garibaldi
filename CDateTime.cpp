//
// Copyright (C) 2017 Project Garibaldi
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software Foundation,
// Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
// 

#include <sstream>
#include <iomanip>
#include "CDateTime.h"

CDateTime::CDateTime(Context* context) : Object(context)
{
	timeAddedOnUpdate = 86400;
	deltaPerUpdate = 1.0f; 
	deltaSinceUpdate = 0.0f;

	year = 1877;
	month = 1;
	day = 1;
	hour = 0;
	minute = 0;
	second = 0;
}

void CDateTime::Update(float delta)
{
	deltaSinceUpdate += delta;
	if (deltaSinceUpdate >= deltaPerUpdate)
	{
		deltaSinceUpdate -= deltaPerUpdate;

		// Update the time
		int timeElapsed = timeAddedOnUpdate;
		int days = timeElapsed / 86400;
		timeElapsed -= days * 86400;
		int hours = timeElapsed / 3600;
		timeElapsed -= hours * 3600;
		int minutes = timeElapsed / 60;
		timeElapsed -= minutes * 60;
		int seconds = timeElapsed;

		SetDay(day + days);
		SetHour(hour + hours);
		SetMinute(minute + minutes);
		SetSecond(second + seconds);

		SendEvent("DateTimeUpdated");
	}
}

void CDateTime::SetDate(int y, int m, int d)
{
	SetYear(y);
	SetMonth(m);
	SetDay(d);
}

void CDateTime::SetTime(int h, int m, int s)
{
	SetHour(h);
	SetMinute(m);
	SetSecond(s);
}

void CDateTime::SetYear(int y)
{
	int prevYear = year;
	year = y;

	if (prevYear != year)
		SendEvent("YearUpdated");
}

void CDateTime::SetMonth(int m)
{
	int prevMonth = month;
	month = m;
	
	if (month > 12)
	{
		SetYear(year + 1);
		month = 1;
	}

	if (prevMonth != month)
		SendEvent("MonthUpdated");
}

void CDateTime::SetDay(int d)
{
	int prevDay = day;
	day = d;

	bool leap = IsLeapYear();
	if ((leap && day > leapDaysInMonth[month-1]) || (!leap && day > daysInMonth[month-1]))
	{
		SetMonth(month + 1);
		day = 1;
	}

	if (prevDay != day)
		SendEvent("DayUpdated");

}

void CDateTime::SetHour(int h)
{
	int prevHour = hour;
	hour = h;

	if (hour > 23)
	{
		SetDay(day + 1);
		hour -= 24;
	}

	if (prevHour != hour)
		SendEvent("HourUpdated");
}

void CDateTime::SetMinute(int m)
{
	minute = m;
	if (minute > 59)
	{
		SetHour(hour + 1);
		minute -= 60;
	}
}

void CDateTime::SetSecond(int s)
{
	second = s;
	if (second > 59)
	{
		SetMinute(minute + 1);
		second -= 60;
	}
}

bool CDateTime::IsLeapYear()
{
	return ((year % 4 == 0 && year % 100 == 0 && year % 400 == 0) || (year % 4 == 0 && year % 100 != 0));
}

String CDateTime::GetDate()
{
	String date(year);
	date += "-";
	date += month;
	date += "-";
	date += day;

	return date;
}

String CDateTime::GetTime()
{
	std::ostringstream ss;
	ss << std::setw(2) << std::setfill('0') << hour << ":";
	ss << std::setw(2) << std::setfill('0') << minute << ":";
	ss << std::setw(2) << std::setfill('0') << second;
	return ss.str().c_str();
}
