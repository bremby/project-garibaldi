//
// Copyright (C) 2017 Project Garibaldi
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software Foundation,
// Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
// 


#include "CPop.h"

CPop::CPop(unsigned province, String name, String religion, String culture, float employed, float literate)
	: province_key(province), name(name), religion(religion), culture(culture), employed(employed), literate(literate)
{
}

bool CPop::operator!=(const CPop a)
{
	return (a.name.Compare(name) != 0) || (a.province_key != province_key) || (a.employed != employed) || (a.literate != literate) || (a.religion.Compare(religion) != 0) || (a.culture.Compare(culture) != 0);
}
