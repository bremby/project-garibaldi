//
// Copyright (C) 2017 Project Garibaldi
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software Foundation,
// Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
//

#pragma once

#include "CPop.h"
#include "CProducer.h"
#include "CStockpile.h"

#include <Urho3D/Container/Str.h>
#include <Urho3D/Math/Color.h>
#include <Urho3D/Math/Vector3.h>

using namespace Urho3D;

class CProvince
{
public:
	CProvince(){};
	CProvince(String name, unsigned colour, Log *log, int centerX, int centerY);

	void Process();

public:
	String name;
	unsigned colour;
	unsigned ownerTag;
	unsigned occupantTag;
	IntVector2 centerI; // real coordinates on the image
	Vector3 centerF; // converted coordinates in the 3D space
	Vector<CPop> pops;
	Log *log;
	CStockpile stockpile;
	Vector<CProducer> producers;
	Vector<unsigned> neighbours;
};
