//
// Copyright (C) 2017 Project Garibaldi
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software Foundation,
// Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
//

#pragma once

#include "CNation.h"
#include "CProvince.h"

#include <Urho3D/Container/Ptr.h>
#include <Urho3D/Container/Str.h>
#include <Urho3D/Math/Vector2.h>
#include <Urho3D/Resource/Image.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Scene/Scene.h>

using namespace Urho3D;

typedef HashMap<unsigned, CProvince> ProvinceMap;
typedef HashMap<unsigned, CNation> NationMap;

class CMap
{
public:
	CMap(Context *c, Scene *s, ResourceCache *rc, ProvinceMap *pm, NationMap *nm)
	 : context_(c), scene_(s), cache_(rc), provinces_(pm), nations_(nm) {};
	int Setup();
	unsigned GetPixelInt(int x, int y);
	bool IsNodeHit(Node *n);

private:
	//IntVector FindProvincePixel(unsigned colour);
	void FillWithColourFromPixel(CProvince &p);
	void FloodFillSimple(int startx, int starty);
	void FloodFillDual(int startx, int starty);

	// our data
	SharedPtr<Image> colouredMap_;
	SharedPtr<Image> politicalMap_;
	unsigned origColour;
	unsigned ownerColour;
	unsigned occupantColour;

	// game related
	ProvinceMap *provinces_;
	NationMap *nations_;

	// Urho3D related
	Context *context_;
	SharedPtr<Scene> scene_;
	SharedPtr<ResourceCache> cache_;
	SharedPtr<Node> terrainNode_;
	SharedPtr<Node> borderNode_;
	SharedPtr<Node> politicalNode_;
};
