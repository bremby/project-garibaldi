//
// Copyright (C) 2017 Project Garibaldi
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software Foundation,
// Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
//

#pragma once

#include <Urho3D/Container/Pair.h>
#include <Urho3D/Container/Vector.h>
#include <Urho3D/IO/Log.h>

#include "CResource.h"

using namespace Urho3D;

typedef Pair<EResource, unsigned> Commodity;

class CStockpile
{
public:
	CStockpile();
	unsigned AddResource(EResource r, unsigned amount);
	unsigned RemResource(EResource r, unsigned amount);
	bool ConsumeAndProduce(Vector<Commodity> inputs, Vector<Commodity> outputs);

public:
	unsigned resources[RES_LAST];
	float res_values[RES_LAST];
	Log *log;
};
