//
// Copyright (C) 2017 Project Garibaldi
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software Foundation,
// Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
//


#include "Main.h"

#include "CBrigade.h"

#include <Urho3D/Container/Vector.h>
#include <Urho3D/Core/CoreEvents.h>
#include <Urho3D/Core/ProcessUtils.h>
#include <Urho3D/Core/StringUtils.h>
#include <Urho3D/Core/Timer.h>
#include <Urho3D/Engine/Application.h>
#include <Urho3D/Engine/Console.h>
#include <Urho3D/Engine/DebugHud.h>
#include <Urho3D/Engine/Engine.h>
#include <Urho3D/Graphics/Camera.h>
#include <Urho3D/Graphics/Graphics.h>
#include <Urho3D/Graphics/Octree.h>
#include <Urho3D/Graphics/Renderer.h>
#include <Urho3D/Graphics/Texture2D.h>
#include <Urho3D/Graphics/Zone.h>
#include <Urho3D/Input/Input.h>
#include <Urho3D/Input/InputEvents.h>
#include <Urho3D/IO/FileSystem.h>
#include <Urho3D/IO/Log.h>
#include <Urho3D/Resource/JSONFile.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Resource/XMLFile.h>
#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Scene/SceneEvents.h>
#include <Urho3D/UI/Button.h>
#include <Urho3D/UI/Cursor.h>
#include <Urho3D/UI/Sprite.h>
#include <Urho3D/UI/Font.h>
#include <Urho3D/UI/Text.h>
#include <Urho3D/UI/UI.h>
#include <Urho3D/UI/UIEvents.h>
#include <Urho3D/Urho2D/AnimatedSprite2D.h>
#include <Urho3D/Urho2D/AnimationSet2D.h>
#include <Urho3D/Urho2D/Sprite2D.h>

#include <Urho3D/DebugNew.h>

#include <climits>
#include <sstream>
#include <iomanip>

// Expands to this example's entry-point
URHO3D_DEFINE_APPLICATION_MAIN(Main)


Main::Main(Context* context) :
	Application(context),
	paused_(false),
	useMouseMode_(MM_FREE)
{
}

void Main::Setup()
{
	// Modify engine startup parameters
	engineParameters_["WindowTitle"] = GetTypeName();
	engineParameters_["LogName"]     = GetSubsystem<FileSystem>()->GetAppPreferencesDir("urho3d", "logs") + GetTypeName() + ".log";
	engineParameters_["FullScreen"]  = false;
	engineParameters_["Headless"]    = false;
	engineParameters_["Sound"]       = false;

	// Construct a search path to find the resource prefix with two entries:
	// The first entry is an empty path which will be substituted with program/bin directory -- this entry is for binary when it is still in build tree
	// The second and third entries are possible relative paths from the installed program/bin directory to the asset directory -- these entries are for binary when it is in the Urho3D SDK installation location
	if (!engineParameters_.Contains("ResourcePrefixPaths"))
		engineParameters_["ResourcePrefixPaths"] = ";../share/Resources;../share/Urho3D/Resources";

	log_ = GetSubsystem<Log>();
	log_->SetLevel(LOG_DEBUG);
}

void Main::Start()
{
	OpenConsoleWindow();

	LoadData();

	CreateUI();

	CreateScene();

	// Set custom window Title & Icon
	SetWindowTitleAndIcon();

	// Enable OS cursor
	GetSubsystem<Input>()->SetMouseVisible(true);

	// Create console and debug HUD
	//CreateConsoleAndDebugHud();

	// Setup the viewport for displaying the scene
	SetupViewport();

	SubscribeToEvents();

	// Set the mouse mode to use in the sample
	//InitMouseMode(MM_FREE); //removing this works for me. how about other platforms?

	wq_ = GetSubsystem<WorkQueue>();
	selectedProvince_ = nullptr;
	selectedUnit_ = nullptr;

	speed_ = 0;
}

void Main::Stop()
{
	engine_->DumpResources(true);
}

void Main::InitMouseMode(MouseMode mode)
{
	useMouseMode_ = mode;

	Input* input = GetSubsystem<Input>();

	input->SetMouseVisible(true);
}

void Main::CreateScene()
{
	scene_ = new Scene(context_);
	scene_->CreateComponent<Octree>();

	// Create camera node
	cameraNode_ = scene_->CreateChild("Camera");
	// Set camera's position
	cameraNode_->SetPosition(Vector3(0.0f, 0.0f, -10.0f));

	camera_ = cameraNode_->CreateComponent<Camera>();
	camera_->SetOrthographic(true);

	Graphics* graphics = GetSubsystem<Graphics>();
	camera_->SetOrthoSize((float)graphics->GetHeight() * PIXEL_SIZE);

	ResourceCache* cache = GetSubsystem<ResourceCache>();

	map_ = new CMap(context_, scene_, cache, &provinces_, &nations_);
	if (map_->Setup() < 0)
		return;

	// Get sprites
	infIcon_ = cache->GetResource<Sprite2D>("Icons/NATO-Infantry.png");
	if (!infIcon_)
		return;

}

void Main::CreateUI()
{
	// Get default style
	ResourceCache* cache = GetSubsystem<ResourceCache>();

	// Set up global UI style into the root UI element
	XMLFile* style = cache->GetResource<XMLFile>("UI/DefaultStyle.xml");
	UI* ui = GetSubsystem<UI>();
	ui->GetRoot()->SetDefaultStyle(style);

	// Create console
	Console* console = engine_->CreateConsole();
	console->SetDefaultStyle(style);
	console->GetBackground()->SetOpacity(0.8f);

	// Create debug HUD.
	DebugHud* debugHud = engine_->CreateDebugHud();
	debugHud->SetDefaultStyle(style);

	//// Create a Cursor UI element because we want to be able to hide and show it at will. When hidden, the mouse cursor will
	//// control the camera, and when visible, it will interact with the UI
	//SharedPtr<Cursor> cursor(new Cursor(context_));
	//cursor->SetStyleAuto();
	//ui->SetCursor(cursor);
	//// Set starting position of the cursor at the rendering window center
	//Graphics* graphics = GetSubsystem<Graphics>();
	//cursor->SetPosition(graphics->GetWidth() / 2, graphics->GetHeight() / 2);

	// Load UI content prepared in the editor and add to the UI hierarchy
	SharedPtr<UIElement> layoutRoot = ui->LoadLayout(cache->GetResource<XMLFile>("UI/main_ui.xml"));
	ui->GetRoot()->AddChild(layoutRoot);

	// Subscribe to button actions (toggle scene lights when pressed then released)
	provinceExplorer_ = layoutRoot->GetChildStaticCast<Window>("ProvinceExplorer", true);
	provinceExplorer_->SetVisible(false);

	provinceView_ = layoutRoot->GetChildStaticCast<Window>("ProvinceWindow", true);
	provinceView_->SetVisible(false);

	unitView_ = layoutRoot->GetChildStaticCast<Window>("UnitWindow", true);
	unitView_->SetVisible(false);

	Text* manPowerValue = unitView_->GetChildStaticCast<Text>("UnitWindowManpower_value", true);
	manPowerValue->SetFixedSize(102, 15);
	manPowerValue->SetTextAlignment(HorizontalAlignment::HA_RIGHT);

	// Create a Text for each resource and a Text for each resource value
	int x = 10;
	int y = 40;
	int width = 320;
	int height = 20;
	int ySpacer = 5;
	for (int r = EResource::RES_COAL; r != EResource::RES_LAST; r++) {
		{
			Text* t = new Text(context_);
			provinceView_->AddChild(t);
			t->SetStyle("Text");
			t->SetPosition(x, y);
			t->SetSize(width / 2 - 20, height);
			t->SetName(getResourceName((EResource)r));
			t->SetText(getResourceName((EResource)r));
		}

		{
			Text* t = new Text(context_);
			provinceView_->AddChild(t);
			t->SetStyle("Text");
			t->SetFixedSize(102, 15);
			t->SetSize(112, 15);
			t->SetPosition(125, y);
			String name(getResourceName((EResource)r));
			String value("_value");
			String s = name + value;
			t->SetName(s);
			t->SetText("Unknown");
			t->SetTextAlignment(HorizontalAlignment::HA_RIGHT);
			log_->URHO3D_LOGDEBUGF("%s Text created", t->GetName().CString());
		}

		y += height + ySpacer;
	}

	String text = dateTime_->GetDate() + " " + dateTime_->GetTime();
	Text *dateTimeText = ui->GetRoot()->GetChildStaticCast<Text>("DateTimeText", true);
	dateTimeText->SetText(text);

	SharedPtr<Button> btnPlay(layoutRoot->GetChildStaticCast<Button>("PlayButton", true));
	SubscribeToEvent(btnPlay, E_PRESSED, URHO3D_HANDLER(Main, HandlePlayPressed));
}

void Main::LoadData()
{
	LoadResources("Maps/resources.json");
	LoadMapConfig("Maps/map.js");
	LoadPops("Maps/pops.json");
	LoadNations("Maps/nations.json");
}

void Main::LoadResources(String file)
{
	JSONValue &root = LoadJSON(file);
	const JSONValue &resources = root.Get("resources");
	const JSONArray &resource_array = resources.GetArray();
	JSONArray::ConstIterator it = resource_array.Begin();
	while (it != resource_array.End())
	{
		// Parse the colour
		const JSONValue &resource = *it;

		// only load if it contains key, name & value
		if (resource.Contains("key") && resource.Contains("name") && resource.Contains("value"))
		{
			unsigned key = (unsigned)resource.Get("key").GetInt();
			String name = resource.Get("name").GetString();
			float value = resource.Get("value").GetFloat();

			Pair<unsigned, CResource> p(key, CResource(key, name, value));
			bool existed;
			resources_.Insert(p, existed);
		}

		++it;
	}

	log_->URHO3D_LOGDEBUGF("Loaded %i resources", resources_.Size());
}

void Main::LoadMapConfig(String file)
{
	JSONValue &root = LoadJSON(file);
	const JSONValue &map = root.Get("map");

	context_->RegisterFactory<CDateTime>();
	dateTime_.StaticCast(context_->CreateObject("CDateTime"));
	int year = map.Get("year").GetInt();
	int month = map.Get("month").GetInt();
	int day = map.Get("day").GetInt();

	dateTime_->SetDate(year, month, day);

	const JSONValue &provinces = map.Get("provinces");
	//log_->URHO3D_LOGDEBUGF("JSON: is array %d", provinces.IsArray());

	// Parse province array
	const JSONArray &prov_array = provinces.GetArray();
	JSONArray::ConstIterator it = prov_array.Begin();
	map_key_min = UINT_MAX;
	map_key_max = 0;
	while (it != prov_array.End()) {
		// Parse the colour
		const JSONValue &prov = *it;
		unsigned total = ParseColour(prov.Get("colour").GetString());
		if (total < map_key_min) map_key_min = total;
		if (total > map_key_max) map_key_max = total;

		// Get province name
		String name = prov.Get("name").GetString();

		CProvince province(name, total, log_, prov.Get("centerX").GetUInt(), prov.Get("centerY").GetUInt());

		if (prov.Contains("owner")) {
			province.ownerTag = prov.Get("owner").GetUInt();
			if (prov.Contains("occupant")) {
				province.occupantTag = prov.Get("occupant").GetUInt();
			} else {
				province.occupantTag = province.ownerTag;
			}
		}

		if (prov.Contains("produces")) {
			log_->URHO3D_LOGDEBUGF("Province has resources: %s", name.CString());
			const JSONValue &produces = prov.Get("produces");
			const JSONArray &production_array = produces.GetArray();
			JSONArray::ConstIterator pit = production_array.Begin();
			while (pit != production_array.End()) {
				CProducer producer;
				if (pit->Contains("in")) {
					ConstJSONObjectIterator in = pit->Get("in").Begin();
					while (in != pit->Get("in").End()) {
						producer.AddInput(getResourceEnum(in->first_.CString()), in->second_.GetUInt());
						++in;
					}
				}
				if (pit->Contains("out")) {
					ConstJSONObjectIterator out = pit->Get("out").Begin();
					while (out != pit->Get("out").End()) {
						producer.AddOutput(getResourceEnum(out->first_.CString()), out->second_.GetUInt());
						++out;
					}
				}
				province.producers.Push(producer);
				++pit;
			}
		}

		// Insert into province db in memory
		Pair<unsigned, CProvince> p(total, province);
		//log_->URHO3D_LOGDEBUGF("inserting province: %s", p.second_.name.CString());
		bool existed;
		provinces_.Insert(p, existed);
		if (existed) {
			log_->URHO3D_LOGERRORF("Map configuration: repeated colour in %s!", name.CString());
		}

		++it;
	}
}

void Main::LoadPops(String file)
{
	JSONValue &root = LoadJSON(file);
	const JSONValue &pops = root.Get("pops");
	const JSONArray &pop_array = pops.GetArray();
	JSONArray::ConstIterator it = pop_array.Begin();
	while (it != pop_array.End())
	{
		// Parse the colour
		const JSONValue &pop = *it;
		if (pop.Contains("province"))
		{
			unsigned province_key = ParseColour(pop.Get("province").GetString());
			String name = pop.Contains("name") ? pop.Get("name").GetString() : "Default Name";
			String religion = pop.Contains("religion") ? pop.Get("religion").GetString() : "Default Religion";
			String culture = pop.Contains("culture") ? pop.Get("culture").GetString() : "Default Culture";
			float employed = pop.Contains("employed") ? pop.Get("employed").GetFloat() : 0.0f;
			float literate = pop.Contains("literate") ? pop.Get("literate").GetFloat() : 0.0f;

			CPop new_pop(province_key, name, religion, culture, employed, literate);

			AddPopToProvince(new_pop, province_key);

			popList_.Push(new_pop);
		}

		++it;
	}

	log_->URHO3D_LOGDEBUGF("Loaded %i pops", popList_.Size());
}

void Main::LoadNations(String file)
{
	JSONValue &root = LoadJSON(file);
	const JSONValue &nations = root.Get("nations");
	const JSONArray &nation_array = nations.GetArray();
	JSONArray::ConstIterator it = nation_array.Begin();
	while (it != nation_array.End())
	{
		const JSONValue &nation = *it;

		if (nation.Contains("tag"))
		{
			unsigned tag = nation.Get("tag").GetUInt();
			String name = nation.Get("name").GetString();
			unsigned politicalColour = ParseColour(nation.Get("colour").GetString());

			Pair<unsigned, CNation> p(tag, CNation(tag, name, politicalColour));
			nations_.Insert(p);
		}

		++it;
	}

	log_->URHO3D_LOGDEBUGF("Loaded %i nations", nations_.Size());
}

unsigned Main::ParseColour(String colour)
{
	String comp_str = colour.Substring(0, 2);
	unsigned red = ToUInt(comp_str, 16);
	comp_str = colour.Substring(2, 2);
	unsigned green = ToUInt(comp_str, 16);
	comp_str = colour.Substring(4, 2);
	unsigned blue = ToUInt(comp_str, 16);
	return 0xff000000 | (blue << 16) | (green << 8) | red;
}

bool Main::CheckResource(String file)
{
	return GetSubsystem<ResourceCache>()->Exists(file);
}

JSONValue& Main::LoadJSON(String file)
{
	ResourceCache* cache = GetSubsystem<ResourceCache>();
	JSONFile *json = cache->GetResource<JSONFile>(file);
	return json->GetRoot();
}

void Main::SetWindowTitleAndIcon()
{
	ResourceCache* cache = GetSubsystem<ResourceCache>();
	Graphics* graphics = GetSubsystem<Graphics>();
	Image* icon = cache->GetResource<Image>("Textures/garibaldi.png");
	graphics->SetWindowIcon(icon);
	graphics->SetWindowTitle("Project Garibaldi");
}

void Main::AddPopToProvince(CPop& pop, unsigned province_key)
{
	if (!provinces_.Contains(province_key))
		return;

	provinces_[province_key].pops.Push(pop);
}

void Main::RemovePopFromProvince(CPop& pop, unsigned province_key)
{
	if (!provinces_.Contains(province_key))
		return;

	provinces_[province_key].pops.Remove(pop);
}

void Main::SetupViewport()
{
	Renderer* renderer = GetSubsystem<Renderer>();

	// Set up a viewport to the Renderer subsystem so that the 3D scene can be seen
	viewport_ = new Viewport(context_, scene_, cameraNode_->GetComponent<Camera>());
	renderer->SetViewport(0, viewport_);

	Zone *zone = renderer->GetDefaultZone();
	zone->SetFogColor(Color(0.2f,0.8f,1.0f));
}

void Main::MoveCamera(float timeStep)
{
	// Do not move if the UI has a focused element (the console)
	if (GetSubsystem<UI>()->GetFocusElement())
		return;

	Input* input = GetSubsystem<Input>();

	// Movement speed as world units per second
	const float MOVE_SPEED = 4.0f;

	// Read WASD keys and move the camera scene node to the corresponding direction if they are pressed
	if (input->GetKeyDown(KEY_W))
		cameraNode_->Translate(Vector3::UP * MOVE_SPEED * timeStep);
	if (input->GetKeyDown(KEY_S))
		cameraNode_->Translate(Vector3::DOWN * MOVE_SPEED * timeStep);
	if (input->GetKeyDown(KEY_A))
		cameraNode_->Translate(Vector3::LEFT * MOVE_SPEED * timeStep);
	if (input->GetKeyDown(KEY_D))
		cameraNode_->Translate(Vector3::RIGHT * MOVE_SPEED * timeStep);

	if (input->GetKeyDown(KEY_PAGEUP))
	{
		Camera* camera = cameraNode_->GetComponent<Camera>();
		camera->SetZoom(camera->GetZoom() * 1.01f);
	}

	if (input->GetKeyDown(KEY_PAGEDOWN))
	{
		Camera* camera = cameraNode_->GetComponent<Camera>();
		camera->SetZoom(camera->GetZoom() * 0.99f);
	}
}

void Main::HandleKeyUp(StringHash eventType, VariantMap& eventData)
{
	using namespace KeyUp;

	int key = eventData[P_KEY].GetInt();

	// Close console (if open) or exit when ESC is pressed
	if (key == KEY_ESCAPE)
	{
		Console* console = GetSubsystem<Console>();
		if (console->IsVisible())
			console->SetVisible(false);
		else
		{
			engine_->Exit();
		}
	}
}

void Main::HandleKeyDown(StringHash eventType, VariantMap& eventData)
{
	using namespace KeyDown;

	int key = eventData[P_KEY].GetInt();

	// Toggle console with F1
	if (key == KEY_F1)
		GetSubsystem<Console>()->Toggle();

	// Toggle debug HUD with F2
	else if (key == KEY_F2)
		GetSubsystem<DebugHud>()->ToggleAll();

	// Common rendering quality controls, only when UI has no focused element
	else if (!GetSubsystem<UI>()->GetFocusElement())
	{
		Renderer* renderer = GetSubsystem<Renderer>();

		// Texture quality
		if (key == '1')
		{
			int quality = renderer->GetTextureQuality();
			++quality;
			if (quality > QUALITY_HIGH)
				quality = QUALITY_LOW;
			renderer->SetTextureQuality(quality);
		}

		// Material quality
		else if (key == '2')
		{
			int quality = renderer->GetMaterialQuality();
			++quality;
			if (quality > QUALITY_HIGH)
				quality = QUALITY_LOW;
			renderer->SetMaterialQuality(quality);
		}

		// Specular lighting
		else if (key == '3')
			renderer->SetSpecularLighting(!renderer->GetSpecularLighting());

		// Shadow rendering
		else if (key == '4')
			renderer->SetDrawShadows(!renderer->GetDrawShadows());

		// Shadow map resolution
		else if (key == '5')
		{
			int shadowMapSize = renderer->GetShadowMapSize();
			shadowMapSize *= 2;
			if (shadowMapSize > 2048)
				shadowMapSize = 512;
			renderer->SetShadowMapSize(shadowMapSize);
		}

		// Shadow depth and filtering quality
		else if (key == '6')
		{
			ShadowQuality quality = renderer->GetShadowQuality();
			quality = (ShadowQuality)(quality + 1);
			if (quality > SHADOWQUALITY_BLUR_VSM)
				quality = SHADOWQUALITY_SIMPLE_16BIT;
			renderer->SetShadowQuality(quality);
		}

		// Occlusion culling
		else if (key == '7')
		{
			bool occlusion = renderer->GetMaxOccluderTriangles() > 0;
			occlusion = !occlusion;
			renderer->SetMaxOccluderTriangles(occlusion ? 5000 : 0);
		}

		// Instancing
		else if (key == '8')
			renderer->SetDynamicInstancing(!renderer->GetDynamicInstancing());

		// Take screenshot
		else if (key == '9')
		{
			Graphics* graphics = GetSubsystem<Graphics>();
			Image screenshot(context_);
			graphics->TakeScreenShot(screenshot);
			// Here we save in the Data folder with date and time appended
			screenshot.SavePNG(GetSubsystem<FileSystem>()->GetProgramDir() + "Data/Screenshot_" +
				Time::GetTimeStamp().Replaced(':', '_').Replaced('.', '_').Replaced(' ', '_') + ".png");
		}
	}
}

void Main::HandleMouseButtonDown(StringHash eventType, VariantMap& eventData)
{
	SubscribeToEvent(E_MOUSEBUTTONUP, URHO3D_HANDLER(Main, HandleMouseButtonUp));
}

void Main::HandleMouseButtonUp(StringHash eventType, VariantMap& eventData)
{
	Input* input = GetSubsystem<Input>();
	Graphics* graphics = GetSubsystem<Graphics>();
	UI* ui = GetSubsystem<UI>();
	// Check the cursor is visible and there is no UI element in front of the cursor
	IntVector2 pos = ui->GetCursorPosition();
	if (ui->GetElementAt(pos, true))
		return;

	Ray cameraRay = camera_->GetScreenRay((float)pos.x_ / graphics->GetWidth(), (float)pos.y_ / graphics->GetHeight());
	PODVector<RayQueryResult> results;
	RayOctreeQuery query(results, cameraRay, RAY_TRIANGLE, 250.0f, DRAWABLE_GEOMETRY);
	scene_->GetComponent<Octree>()->RaycastSingle(query);
	Vector3 hitPos;
	Drawable* hitDrawable;
	if (!results.Size()) return;

	RayQueryResult& result = results[0];
	hitPos = result.position_;
	hitDrawable = result.drawable_;

	/*
	 * Left click on a province selects it and de-selects any selected
	 * unit. Left click on a unit selects the unit and de-selects
	 * the province. Right click on a province or unit moves
	 * the selected unit (if any) to the target province.
	 */

	unsigned uc = map_->GetPixelInt((int) 1000.0f + hitPos.x_ / PIXEL_SIZE, (int) 1000.0f + hitPos.y_ / PIXEL_SIZE);
	ProvinceMap::Iterator it = provinces_.Find(uc);
	if (eventData[MouseButtonDown::P_BUTTON] == MOUSEB_LEFT) {
		/*
		 * Now we can use hitDrawable->GetNode() to compare it to borderNode_.Get().
		 * If we get the same value, we hit the map and we should display
		 * the province. If not, We hit another map node, like a brigade.
		 */
		if (map_->IsNodeHit(hitDrawable->GetNode())) {
			selectedUnit_ = NULL;
			if (it == provinces_.End()) {
				log_->URHO3D_LOGDEBUGF("province not found");

				DeselectUnit();
				DeselectProvince();
			} else {
				log_->URHO3D_LOGDEBUGF("found province: %s", it->second_.name.CString());
				Text *prov_name_text = ui->GetRoot()->GetChildStaticCast<Text>("ProvinceWindowName", true);
				log_->URHO3D_LOGDEBUGF("province: %p pops: %i", prov_name_text, it->second_.pops.Size());
				prov_name_text->SetText(it->second_.name);
				if (input->GetKeyDown(KEY_CTRL)) {
					SharedPtr<Node> infantry(scene_->CreateChild("infantry"));
					infantry->SetPosition(it->second_.centerF);
					StaticSprite2D* staticInfantrySprite = infantry->CreateComponent<StaticSprite2D>();
					staticInfantrySprite->SetSprite(infIcon_);
					staticInfantrySprite->SetOrderInLayer(2); // Draw infantry on top of the terrain and borders

					CBrigade *b = new CBrigade();
					b->name = String("Royal Montengroean infantry brigade");
					b->node = infantry;
					b->sprite = staticInfantrySprite;

					units_.Insert(Pair<Node *, CUnit *>(infantry, b));
				}

				SelectProvince(it->second_);
			}
		} else {
			// Display info on unit(/something else?)
			UnitMap::Iterator it = units_.Find(hitDrawable->GetNode());
			if (it != units_.End()) {
				log_->URHO3D_LOGDEBUGF("unit selected: %s", it->second_->name.CString());

				SelectUnit(*it->second_);
			}
		}
	} else if (eventData[MouseButtonDown::P_BUTTON] == MOUSEB_RIGHT) {
		if (selectedUnit_ && it != provinces_.End()) {
			selectedUnit_->node->SetPosition(it->second_.centerF);
		}
	}

	UnsubscribeFromEvent(E_MOUSEBUTTONUP);
}

void Main::SubscribeToEvents()
{
	// Subscribe key down event
	SubscribeToEvent(E_KEYDOWN, URHO3D_HANDLER(Main, HandleKeyDown));
	// Subscribe key up event
	SubscribeToEvent(E_KEYUP, URHO3D_HANDLER(Main, HandleKeyUp));
	// Subscribe to mouse click
	SubscribeToEvent(E_MOUSEBUTTONDOWN, URHO3D_HANDLER(Main, HandleMouseButtonDown));
	// Subscribe HandleUpdate() function for processing update events
	SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(Main, HandleUpdate));

	SubscribeToEvent("DateTimeUpdated", URHO3D_HANDLER(Main, HandleDateTimeUpdated));
	SubscribeToEvent("YearUpdated", URHO3D_HANDLER(Main, HandleYearUpdated));
	SubscribeToEvent("MonthUpdated", URHO3D_HANDLER(Main, HandleMonthUpdated));
	SubscribeToEvent("DayUpdated", URHO3D_HANDLER(Main, HandleDayUpdated));
	SubscribeToEvent("HourUpdated", URHO3D_HANDLER(Main, HandleHourUpdated));
}

void Main::HandleUpdate(StringHash eventType, VariantMap& eventData)
{
	using namespace Update;

	// Take the frame time step, which is stored as a float
	float timeStep = eventData[P_TIMESTEP].GetFloat();

	// Move the camera, scale movement with time step
	MoveCamera(timeStep);

	// 0 = paused
	if (speed_ > 0)
	{
		dateTime_->Update(timeStep);
	}
}

void Main::HandleDateTimeUpdated(StringHash eventType, VariantMap& eventData)
{
	String text = dateTime_->GetDate() + " " + dateTime_->GetTime();
	UI* ui = GetSubsystem<UI>();
	Text *dateTimeText = ui->GetRoot()->GetChildStaticCast<Text>("DateTimeText", true);
	dateTimeText->SetText(text);
}

void Main::HandleYearUpdated(StringHash eventType, VariantMap& eventData)
{
	log_->URHO3D_LOGDEBUGF("YearUpdated event fired: %i Leap Year: %s", dateTime_->GetYear(), dateTime_->IsLeapYear() ? "Yes" : "No");
}

void Main::HandleMonthUpdated(StringHash eventType, VariantMap& eventData)
{
	log_->URHO3D_LOGDEBUGF("MonthUpdated event fired: %i", dateTime_->GetMonth());
}

void processProvinces(const WorkItem *wi, unsigned tn)
{
	Main::ProvinceMap::Iterator it = ((Main::ProvinceMap *)wi->aux_)->Find((unsigned) (uintptr_t) wi->start_);
	Main::ProvinceMap::Iterator end = ((Main::ProvinceMap *)wi->aux_)->Find((unsigned) (uintptr_t) wi->end_);

	while (it != end) {
		it->second_.Process();
		++it;
	}
}

void Main::HandleDayUpdated(StringHash eventType, VariantMap& eventData)
{
	log_->URHO3D_LOGDEBUGF("DayUpdated event fired: %i", dateTime_->GetDay());

	AddProvinceProcessingJobs();

	UpdateProvinceWindowValues();
}

void Main::HandleHourUpdated(StringHash eventType, VariantMap& eventData)
{
	log_->URHO3D_LOGDEBUGF("HourUpdated event fired: %i", dateTime_->GetHour());
}

void Main::HandlePlayPressed(StringHash eventType, VariantMap& eventData)
{
	UI* ui = GetSubsystem<UI>();
	Text *playText = ui->GetRoot()->GetChildStaticCast<Text>("PlayButtonText", true);

	if (speed_ == 0)
	{
		speed_ = 1;
		playText->SetText("Pause");
	}
	else
	{
		speed_ = 0;
		playText->SetText("Play");
	}

	log_->URHO3D_LOGDEBUGF("Speed set to: %i", speed_);
}

void Main::AddProvinceProcessingJobs()
{
	unsigned nt = wq_->GetNumThreads() - 1; // use all but one thread
	if (nt < 1) nt = 1; // but at least one

	unsigned bs = (map_key_max - map_key_min) / nt + 1; // +1 to avoid problems when the key difference is not divisible by nt
	unsigned start = map_key_min;

	for (int i = 0; i < nt; ++i) {
		SharedPtr<WorkItem> wi = wq_->GetFreeItem();
		wi->workFunction_ = processProvinces;
		wi->start_ = (void *) (uintptr_t) start; // is the double cast necessary?
		start += bs;
		wi->end_ = (void *) (uintptr_t) start;
		wi->aux_ = &provinces_;
		wq_->AddWorkItem(wi);
	}
}

void Main::UpdateProvinceWindowValues()
{
	if (selectedProvince_ == nullptr)
		return;

	// Update each resource text value
	for (int r = EResource::RES_COAL; r != EResource::RES_LAST; r++) {
		String resourceName(getResourceName((EResource)r));
		String value("_value");
		String s = resourceName + value;

		Text* t = provinceView_->GetChildStaticCast<Text>(s, true);
		std::ostringstream ss;
		ss << selectedProvince_->stockpile.resources[r];
		t->SetText(ss.str().c_str());
	}
}

void Main::SelectProvince(CProvince& province)
{
	// Set the province
	selectedProvince_ = &province;

	// Update the UI
	UpdateProvinceWindowValues();

	// Display appropriate UI
	provinceView_->SetVisible(true);
	unitView_->SetVisible(false);
}

void Main::DeselectProvince()
{
	selectedProvince_ = nullptr;
	provinceView_->SetVisible(false);
}

void Main::UpdateUnitWindowValues()
{
	if (selectedUnit_ == nullptr)
		return;

	Text* t = unitView_->GetChildStaticCast<Text>("UnitWindowName", true);
	t->SetText(selectedUnit_->name);

	t = unitView_->GetChildStaticCast<Text>("UnitWindowManpower_value", true);
	std::ostringstream ss;
	ss << selectedUnit_->manpower;
	t->SetText(ss.str().c_str());
}

void Main::SelectUnit(CUnit& unit)
{
	selectedUnit_ = &unit;

	// Update UI
	UpdateUnitWindowValues();

	// Display appropriate UI
	provinceView_->SetVisible(false);
	unitView_->SetVisible(true);
}

void Main::DeselectUnit()
{
	selectedUnit_ = nullptr;
	unitView_->SetVisible(false);
}
